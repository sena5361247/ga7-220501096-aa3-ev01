/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controller;

import Configuracion.Conexion;
import Entity.Bateria;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author BaironB
 */
@Controller
public class Controlador {
    Conexion con = new Conexion();
    JdbcTemplate jdbcTemplate= new JdbcTemplate (con.Conectar());
    ModelAndView mav = new ModelAndView();
    int id;
    List datos;
    
    @RequestMapping("index.htm")
    public ModelAndView Listar(){
        String sql = "select * from bateria";
        datos = this.jdbcTemplate.queryForList(sql);
        mav.addObject("lista", datos);
        mav.setViewName("index");
        return mav;
    }
    
    @RequestMapping(value="agregar.htm", method=RequestMethod.GET)
    public ModelAndView Agregar(){
       mav.addObject(new Bateria());
       mav.setViewName("agregar");
       return mav;
    }
    
    @RequestMapping(value="agregar.htm", method=RequestMethod.POST)
    public ModelAndView Agregar(Bateria b){
       String sql = "insert into bateria (Unidad,Cantidad,No_Parte,Descripcion,Voltaje,Corriente)values (?,?,?,?,?,?)";
       this.jdbcTemplate.update(sql,b.getUnidad(),b.getCantidad(),b.getNo_Parte(),b.getDescripcion(),b.getVoltaje(),b.getCorriente());
       return new ModelAndView("redirect:/index.htm");
    }
    
    @RequestMapping(value="editar.htm", method=RequestMethod.GET)
    public ModelAndView Editar(HttpServletRequest request){
       id=Integer.parseInt(request.getParameter("id_bateria"));
       String sql = "select * from bateria where id_bateria ="+id;
       datos=this.jdbcTemplate.queryForList(sql);
       mav.addObject("lista",datos);
       mav.setViewName("editar");
       return mav;
    }
    
    @RequestMapping(value="editar.htm", method=RequestMethod.POST)
    public ModelAndView Editar (Bateria b){
       String sql = "update bateria set Unidad=?,Cantidad=?,No_Parte=?,Descripcion=?,Voltaje=?,Corriente=? where  id_bateria=?";
       this.jdbcTemplate.update(sql,b.getUnidad(),b.getCantidad(),b.getNo_Parte(),b.getDescripcion(),b.getVoltaje(),b.getCorriente(),id);
       return new ModelAndView("redirect:/index.htm");
    }    
    
    @RequestMapping("delete.htm")
    public ModelAndView Delete(HttpServletRequest request){
       id=Integer.parseInt(request.getParameter("id_bateria"));
       String sql = "delete from bateria where id_bateria="+id;
       this.jdbcTemplate.update(sql);       
       return new ModelAndView("redirect:/index.htm");
    }



    
}
