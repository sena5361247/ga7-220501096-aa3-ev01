/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Entity;

/**
 *
 * @author BaironB
 */
public class Bateria {
    int id;
    String Unidad;
    int Cantidad;
    String No_Parte;
    String Descripcion;
    int Voltaje;        
    int Corriente;

    public Bateria() {
    }

    public Bateria(int id,int Cantidad, String Unidad, String No_Parte, String Descripcion, int Voltaje, int Corriente) {
        this.id = id;
        this.Unidad = Unidad;
        this.Cantidad = Cantidad;
        this.No_Parte = No_Parte;
        this.Descripcion = Descripcion;
        this.Voltaje = Voltaje;
        this.Corriente = Corriente;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUnidad() {
        return Unidad;
    }

    public void setUnidad(String Unidad) {
        this.Unidad = Unidad;
    }

    public int getCantidad() {
        return Cantidad;
    }

    public void setCantidad(int Cantidad) {
        this.Cantidad = Cantidad;
    }

    public String getNo_Parte() {
        return No_Parte;
    }

    public void setNo_Parte(String No_Parte) {
        this.No_Parte = No_Parte;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public int getVoltaje() {
        return Voltaje;
    }

    public void setVoltaje(int Voltaje) {
        this.Voltaje = Voltaje;
    }

    public int getCorriente() {
        return Corriente;
    }

    public void setCorriente(int Corriente) {
        this.Corriente = Corriente;
    }
            
   
    
}
