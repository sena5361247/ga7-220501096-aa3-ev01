/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Configuracion;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 *
 * @author BaironB
 */
public class Conexion {
  
    public DriverManagerDataSource Conectar(){
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/photovoltaics");
        dataSource.setUsername("root");
        dataSource.setPassword("");
        return dataSource;
    }
}

