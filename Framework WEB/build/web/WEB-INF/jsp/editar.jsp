<%-- 
    Document   : editar
    Created on : Mar 2, 2024, 1:19:46 PM
    Author     : BaironB
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <title>Spring Web</title>
    </head>
    <body>
        <div class="container mt-4 col-lg-4">
          <div class="card border-info">
              <div class="card header bg-info">
                  <h4> Nuevo Registro</h4>    
              </div>    
              <div class="card-body">
                  <form method="POST">
                      <label> Unidad </label>
                      <input type="text" name="Unidad" class="form-control" value="${lista[0].Unidad}">
                      <label> Cantidad </label>
                      <input type="text" name="Cantidad" class="form-control" value="${lista[0].Cantidad}">
                      <label> No_Parte </label>
                      <input type="text" name="No_Parte" class="form-control" value="${lista[0].No_Parte}">
                      <label> Descripcion </label>
                      <input type="text" name="Descripcion" class="form-control" value="${lista[0].Descripcion}">
                      <label> Voltaje </label>
                      <input type="text" name="Voltaje" class="form-control" value="${lista[0].Voltaje}">
                      <label> Corriente </label>
                      <input type="text" name="Corriente" class="form-control" value="${lista[0].Corriente}">
                      <input type="submit" value="Actualizar" class="btn btn-success">
                      <a href="index.htm">Regresar</a>
                  </form>
              </div>
          </div>
        </div>    
    </body>
</html>
