<%-- 
    Document   : Agregar
    Created on : Mar 2, 2024, 9:28:03 AM
    Author     : BaironB
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <title>Spring Web</title>
    </head>
    <body>
        <div class="container mt-4 col-lg-4">
          <div class="card border-info">
              <div class="card header bg-info">
                  <h4> Nuevo Registro</h4>    
              </div>    
              <div class="card-body">
                  <form method="POST">
                      <label> Unidad </label>
                      <input type="text" name="Unidad" class="form-control">
                      <label> Cantidad </label>
                      <input type="text" name="Cantidad" class="form-control">
                      <label> No_Parte </label>
                      <input type="text" name="No_Parte" class="form-control">
                      <label> Descripcion </label>
                      <input type="text" name="Descripcion" class="form-control">
                      <label> Voltaje </label>
                      <input type="text" name="Voltaje" class="form-control">
                      <label> Corriente </label>
                      <input type="text" name="Corriente" class="form-control">
                      <input type="submit" value="Agregar" class="btn btn-success">
                      <a href="index.htm">Regresar</a>
                  </form>
              </div>
          </div>
        </div>    
    </body>
</html>
