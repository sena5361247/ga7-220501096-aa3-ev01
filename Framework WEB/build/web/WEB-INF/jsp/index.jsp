<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <title>Spring Web</title>
  </head>
  <body>
      <div class="container mt-4">
          <div class="card border-info">
              <div class="card-header bg-info text-white">
                  <a class="btn btn-primary" href="agregar.htm">Nuevo Registro</a>    
              </div>    
              <div class="card-body">
                  <table class="table table-hover">
                      <thead>
                          <tr>
                              <th>Id Bateria</th>
                              <th>Unidad</th>
                              <th>Cantidad</th>
                              <th>No Parte</th>
                              <th>Descripcion</th>
                              <th>Voltaje</th>
                              <th>Corriente</th>
                              <th>Acciones</th>
                          </tr>
                      </thead>
                      <tbody>
                          <c:forEach var="dato" items="${lista}">
                          <tr>
                              <td>${dato.id_bateria}</td>
                              <td>${dato.Unidad}</td>
                              <td>${dato.Cantidad}</td>
                              <td>${dato.No_Parte}</td>
                              <td>${dato.Descripcion}</td>
                              <td>${dato.Voltaje}</td>
                              <td>${dato.Corriente}</td>
                              <td>
                                  <a href="editar.htm?id_bateria=${dato.id_bateria}" class="btn btn-warning">Editar </a>
                                  <a href="delete.htm?id_bateria=${dato.id_bateria}" class="btn btn-danger">Borrar </a>
                              </td>
                             </c:forEach>
                          </tr>
                      </tbody>
                  </table>


              </div>    
          </div>    
      </div>
    
  </body>
</html>
